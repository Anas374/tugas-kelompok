<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;


class kategoricontroller extends Controller
{
    public function create(){
        return view('kategori.create');
        }
        public function store(Request $request){
    
            $request->validate([
                'nama' => 'required',
                'dekripsi' => 'required'
            ]);
    
            DB::table('kategori')->insert([
                'nama' => $request['nama'],
                'dekripsi' => $request['dekripsi']
            ]);
            
            return redirect('/kategori');    
        }
        public function index(){
            $kategori = DB::table('kategori')->get();
    
            return view('kategori.index', compact('kategori'));
        }
    
        public function show($id){
            $kategori = DB::table('kategori')->where('id',$id)->first();
            return view('kategori.show', compact('kategori'));
        }
    
           public function edit($id){
            $kategori = DB::table('kategori')->where('id',$id)->first();
            return view('kategori.edit', compact('kategori'));
            
           }
                public function update($id, Request $request){
                    $request->validate([
                        'nama' => 'required',
                        'dekripsi' => 'required',
                    ]);

                $query = DB::table('kategori')
                ->where ('id',$id)
                ->update([
                    'nama'=>$request['nama'],
                    'dekripsi'=>$request['dekripsi']
                ]);
    
                return redirect('/kategori');
            }
                    public function destroy($id){
                        DB::table('kategori')->where('id',$id)->delete();
    
                        return redirect('/kategori');
                    }
    }