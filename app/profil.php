<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profil extends Model
{
    protected $table = 'profil';
    protected $fillable = ['umur','alamat','bio','user_id'];
}
