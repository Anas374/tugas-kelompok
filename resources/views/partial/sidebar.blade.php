<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
      <img src="{{asset('admin/dist/img/AdminLTELogo.png')}}"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Portal Berita</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('admin/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          @guest
          <a href="#" class="d-block">Belum Masuk</a>  
          @endguest

          @auth
          <a href="#" class="d-block">{{Auth::user()->name}}</a>  
          @endauth
          
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          
             
          <li class="nav-item"> 
            <a href="data-table" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Data Table
                
              </p>
            </a>
          </li>
          <li class="nav-item"> 
            <a href="/berita" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Berita
                
              </p>
            </a>

            @auth    
            
          </li><li class="nav-item"> 
            <a href="/kategori" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Kategori
                
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="nav-icon fas fa-sign-out"></i>
                                <p>
                                    
                                    logout

                                  </p>
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
          </li>
          @endauth
          
          @guest
        </li><li class="nav-item"> 
          <a href="/login" class="nav-link bg-primary">
            <i class="nav-icon far fa-sign-in" aria-hidden="true"></i>
            <p>
              Login
              
            </p>
          </a>
          @endguest
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>