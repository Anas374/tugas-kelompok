@extends('layout.master')

@section('judul')
Halaman kategori
@endsection

@section('content')
<a href="/kategori/create" class="btn btn-success mb-3">Tambah Data</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">nama</th>
        <th scope="col">dekripsi</th>
        <th scope="col">Action</th>
      
      </tr>
    </thead>
    <tbody>
            @forelse ($kategori as $key=>$item)
                <tr>
                    <td>{{$key +1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->dekripsi}}</td>
                   
                
                <td>
                  <form action="/kategori/{{$item->id}}" method="POST">

                  
                    <a href="/kategori/{{$item->id}}" class="btn btn-info btn-sm1">Detail</a>
                    <a href="/kategori/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    
                        
                        @method('delete')
                        @csrf
                            <input type="submit" class="btn btn-danger btn-sm " value="delete">
                        
                    </form>
                </td>
                </tr>
            @empty
            <tr>

            <td>data masih kosong</td>
            </tr>

            

            @endforelse
    </tbody>
  </table>

  @endsection