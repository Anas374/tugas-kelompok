@extends('Layout.master')

@section('judul')
Halaman edit berita
@endsection

@section('content')

<form action="/berita/{{$berita->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>judul Berita</label>
      <input type="text" value="{{$berita->judul}}" name="judul" class="form-control">
    </div>

    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <div class="form-group">
      <label>content</label>
     <textarea name="content" class="form-control" cols="30" rows="10">{{$berita->content}}</textarea>
    
    </div>

    @error('content')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

<div class="form-group">
    <label>kategori</label>
   <select name="kategori_id" class='form-control' id="">
       
    <option value="">...Pilih Kategori...</option>
    @foreach ($kategori as $item)
    @if ($item ===$berita->kategori_id)

    <option value="{{$item->id}}" selected>{{$item->nama}}</option>   
        
    @else
        
    <option value="{{$item->id}}">{{$item->nama}}</option>   
        
    @endif
    
         
    @endforeach

   </select>
  </div>

  @error('kategori_id')
    <div class="alert alert-danger">{{ $message }}
    
    </div>
  @enderror

 

<div class="form-group">
    <label>Thumbnail</label>
   <input type="file" name="thumbnail" class="form-control">
  </div>

  @error('thumbnail')
  <div class="alert alert-danger">{{ $message }}</div>
@enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>

  </form>

@endsection

